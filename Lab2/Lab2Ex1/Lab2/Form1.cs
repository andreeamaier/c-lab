﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Form1 : Form
    {
        public static String usernameText = "";
        public static String passwordText = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            LoggedIn form2 = new LoggedIn();
            usernameText = username.Text;
            this.Hide();
            form2.ShowDialog();   // deschide cea de a doua forma
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.IO.StreamReader streamReader = new System.IO.StreamReader("login");
            String line;
            if ((line = streamReader.ReadLine()) != null)
            {
                String[] log = line.Split( );
                usernameText = log[0];
                username.Text = usernameText;
                password.Text = log[1];
            }
        }
    }
}

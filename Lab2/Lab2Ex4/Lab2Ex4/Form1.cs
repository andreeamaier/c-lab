﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2Ex4
{
    public partial class Form1 : Form
    {
        public const String path = "C:\\Users\\andre\\source\\repos\\C#Apps\\Lab2\\Lab2Ex4";
        public String str = "";
        public Form1()
        {
            InitializeComponent();
            tabControl1.SelectedIndexChanged += new EventHandler(TabControl1_SelectedIndexChanged);

        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabPage1 == tabControl1.SelectedTab)
            {
                textBox1.Hide();
            }
            if (tabPage2 == tabControl1.SelectedTab)
            {
                pictureBox1.Hide();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CitiesLoad();
        }


        private void CitiesLoad()
        {
            int i = 0;
            String[] cities =  { "Barcelona", "Cluj - Napoca", "Praga", "Roma" };
            while (i < cities.Length) {
                listBox1.Items.Add(cities[i]);
                i++;
            }
        }

        private void LoadImages(int index)
        {
            Bitmap img = new Bitmap(path + "\\" + index + ".jpg");
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Image = (Image)img;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            LoadImages(index + 1);
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
        }

        private void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            /*String str = "";
            if (radioButton1.Checked) str = str + radioButton1.Text;
            if (radioButton2.Checked) str = str + radioButton2.Text;
            if (radioButton3.Checked) str = str + radioButton3.Text;
            if (radioButton4.Checked) str = str + radioButton4.Text;
            */
            textBox1.Text = str;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
                str = (sender as RadioButton).Text;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
                str = (sender as RadioButton).Text;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
            {
                if (radioButton2.Checked || radioButton3.Checked)
                {
                    textBox1.Text = str + (sender as RadioButton).Text;
                }
            }
                
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                if (radioButton2.Checked || radioButton3.Checked)
                {
                    textBox1.Text = str + (sender as RadioButton).Text;
                }
            }
        }
    }
}

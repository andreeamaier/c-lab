﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2Ex3
{
    public partial class Form1 : Form
    {
        Double a, b, r;

        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            getInput();
            r = a + b;
            result.Text = r.ToString();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            getInput();
            r = a - b;
            result.Text = r.ToString();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            getInput();
            r = a * b;
            result.Text = r.ToString();
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            getInput();
            r = a / b;
            result.Text = r.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void getInput()
        {
            a = Double.Parse(input1.Text);
            b = Double.Parse(input2.Text);
        }
    }
}

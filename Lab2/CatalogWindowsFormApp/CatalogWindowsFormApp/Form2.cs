﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CatalogWindowsFormApp
{
    public partial class Form2 : Form
    {
        private String listBoxText = "";

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = "Hello, " + Form1.username + "!";
            label2.Hide();
            label3.Hide();
            label4.Hide();
            textBox1.Hide();
            groupBox1.Hide();
            button2.Hide();
            listBox1.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String studentName = textBox1.Text;
            listBox1.Items.Add(studentName + "  " + listBoxText);
            textBox1.Text = "";
            listBoxText = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label2.Show();
            label3.Show();
            label4.Show();
            textBox1.Show();
            groupBox1.Show();
            button2.Show();
            listBox1.Show();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                listBoxText = listBoxText + (sender as RadioButton).Text;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                listBoxText = listBoxText + (sender as RadioButton).Text;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                listBoxText = listBoxText + (sender as RadioButton).Text;
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
            {
                listBoxText = listBoxText + (sender as RadioButton).Text;
            }
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                listBoxText = listBoxText + (sender as RadioButton).Text;
            }
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton6.Checked)
            {
                listBoxText = listBoxText + (sender as RadioButton).Text;
            }
        }
    }
}

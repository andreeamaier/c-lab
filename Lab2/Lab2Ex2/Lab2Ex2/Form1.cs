﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2Ex2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StreamReader streamReader = new StreamReader("lista.txt");
            String line = null;

            while ((line = streamReader.ReadLine()) != null)
            {
                listBox1.Items.Add(line);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var item in listBox1.SelectedItems)
            {
                listBox2.Items.Add(item);              
            }

            listBox1.ClearSelected();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           foreach (string item in listBox2.SelectedItems.OfType<string>().ToList())
                listBox2.Items.Remove(item);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

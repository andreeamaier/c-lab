﻿using System;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int option = 1;

            while (option != 0)
            {

                displayMenu();
                option = int.Parse(Console.ReadLine());

                switch (option)
                {
                    case 1:
                        {
                            int n;
                            int[] fibo;
                            Console.Write("Introduceti numarul: ");
                            n = int.Parse(Console.ReadLine());
                            fibo = Fibonacci(n);
                            Console.WriteLine("Primii {0} termeni ai sirului sunt: ", n);
                            for (int i = 0; i < n; i++)
                            {
                                Console.Write("{0} ", fibo[i]);
                            }
                            break;
                        }
                    case 2:
                        {
                            int a, b;
                            Console.Write("a= ");
                            a = int.Parse(Console.ReadLine());
                            Console.Write("b= ");
                            b = int.Parse(Console.ReadLine());
                            OperatiiBiti(a, b);
                            break;
                        }
                    case 3:
                        {
                            float a, b;
                            Console.Write("c= ");
                            a = float.Parse(Console.ReadLine());
                            Console.WriteLine("=> f= {0}", CelsiusToF(a));
                            Console.Write("f= ");
                            b = float.Parse(Console.ReadLine());
                            Console.WriteLine("=> c= {0}", FToCelsius(b));
                            break;
                        }
                    case 4:
                        {
                            float gr, inal, ani;
                            char gen;
                            Console.Write("greutate= ");
                            gr = float.Parse(Console.ReadLine());
                            Console.Write("inaltime= ");
                            inal = float.Parse(Console.ReadLine());
                            Console.Write("gen= ");
                            gen = char.Parse(Console.ReadLine());
                            Console.Write("varsta= ");
                            ani = float.Parse(Console.ReadLine());
                            Console.WriteLine("Greutatea ideala este: {0}", GreutateaIdeal(gr, inal, gen, ani));
                            break;
                        }
                    case 5:
                        {
                            int n;
                            Console.Write("n= ");
                            n = int.Parse(Console.ReadLine());
                            Console.WriteLine("Introduceti cele {0} numere", n);
                            int[] sir = new int[n];
                            for (int i = 0; i < n; i++)
                            {
                                sir[i] = int.Parse(Console.ReadLine());
                            }
                            Console.WriteLine("Media aritmetica = {0}", MediaAritmetica(sir, n));
                            Console.WriteLine("Media geometricaa = {0}", MediaGeometrica(sir, n));
                            break;
                        }
                    case 0:
                        {
                            break;
                        }

                }

            }
            
            Console.ReadLine();


        }

        static int[] Fibonacci(int n)
        {
            int[] fibo = new int[n+1];
            fibo[0] = 1;
            fibo[1] = 1;
            for (int i =2; i<=n; i++)
            {
                fibo[i] =fibo[i-1]+fibo[i-2];
            }
            return fibo;
        }

        static void OperatiiBiti(int a, int b)
        {
            Console.WriteLine("SAU: {0}", a | b);
            Console.WriteLine("SI: {0}", a & b);
            Console.WriteLine("XOR: {0}", a ^ b);
        }

        static float CelsiusToF(float cDeg)
        {
            return cDeg * 9 / 5 + 32;
        }

        static float FToCelsius(float fDeg)
        {
            return (fDeg - 32) * 5 / 9;
        }

        static float GreutateaIdeal(float kg, float inaltime, char gen, float varsta)
        {
            float grIdeal = 0;
            if (gen.Equals('m'))
            {
                grIdeal = (inaltime - 100 - ((inaltime - 150)/4))+((varsta-20)/4);
            } else if (gen.Equals('f'))
            {
                grIdeal = (inaltime - 100 - ((inaltime - 150) / 2.5F)) + ((varsta - 20) / 6);
            }
            return grIdeal;
        }

        static float MediaAritmetica(int[] sir, int n)
        {
            int sum = 0;
            for (int i=0; i<n; i++)
            {
                sum += sir[i];
            }
            return (float)sum / n;
        }

        static double MediaGeometrica(int[] sir, int n)
        {
            double prod = 1;
            for (int i = 0; i < n; i++)
            {
                prod *= sir[i];
            }
            return Math.Pow(prod, (float)1 / n);
        }

        static void displayMenu()
        {
            Console.WriteLine("\n\n Alege exercitiu: ");
            Console.WriteLine("1. Fibbonaci");
            Console.WriteLine("2. Operatori binari");
            Console.WriteLine("3. Conversie grade");
            Console.WriteLine("4. Greutate ideala");
            Console.WriteLine("5. Media geometrica si aritmetica");
            Console.WriteLine("0. Iesire meniu");
            Console.WriteLine("");
            Console.WriteLine("Optiune: ");
        }
    }
}
